import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import {User} from './user';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService
{
    private usersUrl = 'app/users';
    private headers = new Headers({'Content-Type': 'application/json'});

    private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
}

    constructor(private http: Http){}

    getUsers(): Promise <User[]>
    {
        return this.http.get(this.usersUrl)
        .toPromise()
        .then(response => response.json().data as User[])
        .catch(this.handleError);
     }

     getUsersSlowly(): Promise<User[]>{
         return new Promise<User[]>(resolve =>
         setTimeout(resolve,2000))
         .then(()=> this.getUsers());
         
     }

     getUser(id: number): Promise<User>{
         return this.getUsers()
         .then(users => users.find(user => user.id === id));
     }

     update(user: User ): Promise<User>{
         const url = `${this.usersUrl}/${user.id}`;
         return this.http
         .put(url, JSON.stringify(user), {headers: this.headers})
         .toPromise()
         .then(() => user)
         .catch(this.handleError);
     }
         create(name:string): Promise<User>{
             return this.http
             .post(this.usersUrl, JSON.stringify({name: name}), {headers:this.headers})
             .toPromise()
             .then(res => res.json().data)
             .catch(this.handleError)
         }

         delete(id:number): Promise<void>{
             const url= `${this.usersUrl}/${id}`;

             return this.http.delete(url, {headers: this.headers})
             .toPromise()
             .then(()=> null)
             .catch(this.handleError);
         }

     }


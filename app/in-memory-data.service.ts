import { InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService{
    createDb(){
        let users = [
    { id: 1, name: 'Gio Medina' },
    { id: 2, name: 'Kevin Bahia' },
    { id: 3, name: 'JJ Licuanan' },
    { id: 4, name: 'Jessie Balanag' },
    { id: 5, name: 'Stephanie Cerado' },
    { id: 6, name: 'Edward Bongabong' },
    { id: 7, name: 'Christian TQ' }
        ]    ;    
    
    return {users};
                }
}
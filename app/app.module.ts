import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { HttpModule} from '@angular/http';

import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';

import {UserDetailComponent} from './user-detail.component';
import { AppComponent }  from './app.component';
import {UsersComponent} from './users.component';
import {DashboardComponent} from './dashboard.component';
import {UserService} from './user.service';
import {UserSearchComponent} from './user-search.component'

import {AppRoutingModule} from './app-routing.module';
import './rxjs-extensions';

@NgModule({
  imports:      [ BrowserModule, 
                  FormsModule,
                  AppRoutingModule,
                  HttpModule,
                  InMemoryWebApiModule.forRoot(InMemoryDataService)  
                 ],
  declarations: [ AppComponent,
                  UserDetailComponent,
                  UsersComponent,
                  DashboardComponent,
                  UserSearchComponent]
                  ,
  providers: [UserService],

  bootstrap:    [ AppComponent ]
})
export class AppModule { 

}

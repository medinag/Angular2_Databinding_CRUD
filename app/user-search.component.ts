import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

import {UserSearchService} from './user-search.service';
import {User} from './user';

@Component({
    moduleId: module.id,
    selector: 'user-search',
    templateUrl:'user-search.component.html',
    styleUrls:['user-search.component.css'],
    providers:[UserSearchService]
})

export class UserSearchComponent implements OnInit{
    users: Observable<User[]>;
    private searchTerms = new Subject<string>();

constructor(
    private userSerachService: UserSearchService,
    private router: Router){}


    search(term: string): void {
        this.searchTerms.next(term);
    }


    ngOnInit(): void{
        this.users = this.searchTerms
        .debounceTime(100)
        .distinctUntilChanged()
        .switchMap(term=> term
        ?this.userSerachService.search(term)
        :Observable.of<User[]>([]))
        .catch(error => {
            console.log(error);
            return Observable.of<User[]>([])
        });
    }

    gotoDetail(user:User): void{
        let link=['/detail', user.id];
        this.router.navigate(link);
    }
}